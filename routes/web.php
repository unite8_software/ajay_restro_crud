<?php

use Illuminate\Support\Facades\Route;

Route::group(['middleware' => 'web'], function() {

    Route::get('/','RestroController@index');
    Route::get("/list",'RestroController@list');
    Route::post('/add','RestroController@add');
    Route::view('/add','add');
    Route::get('/edit/{id}','RestroController@edit');
    Route::get('/delete/{id}','RestroController@delete');
    Route::post('update/{id}','RestroController@update');
    Route::view('register','register');
    Route::post('register','RestroController@register');
    Route::view('login','login');
    Route::post('login','RestroController@login');
    Route::get('logout','RestroController@logout');
});