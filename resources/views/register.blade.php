@extends('layout')

@section('content')
<div>
    <h1>User Register Page </h1>
    <form action="register" method="post">
    @csrf
        <div class="col-sm-12">
            <div class="row">
                <div class="col-sm-6 mb-3">
                    <label>Name</label>
                    <input type="text" name="name" class="form-control" placeholder="Enter name">
                </div>              
                <div class="col-sm-6 mb-3">
                    <label>Contact</label>
                    <input type="text" name="contact" class="form-control" placeholder="Enter contact">
                </div>
                <div class="col-sm-6 mb-3">
                    <label>Email</label>
                    <input type="email" name="email" class="form-control" placeholder="Enter email">
                </div>
                <div class="col-sm-6 mb-3">
                    <label>Password</label>
                    <input type="password" name="password" class="form-control" placeholder="Enter password">
                </div>  
            </div>
        </div>
        <button type="submit" class="btn btn-primary">Register</button>
    </form>
</div>
@stop