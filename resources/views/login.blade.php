@extends('layout')

@section('content')
<div>
    <h1>User Login Page </h1>
    @if(Session::get('status'))
    <div class="alert alert-danger alert-dismissible fade show" role="alert">
        <strong> {{Session::get('status')}} </strong>
        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
    </div>
    @endif
    <form action="login" method="post">
        @csrf
        <div class="col-sm-12">
            <div class="row">
                <div class="col-sm-6 mb-3">
                    <label>Email</label>
                    <input type="email" name="email" class="form-control" placeholder="Enter email">
                </div>
                <div class="col-sm-6 mb-3">
                    <label>Password</label>
                    <input type="password" name="password" class="form-control" placeholder="Enter password">
                </div>
            </div>
        </div>
        <button type="submit" class="btn btn-primary">Login</button>
    </form>
</div>
@stop