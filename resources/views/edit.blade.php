@extends('layout')

@section('content')
<div>
    <h1>Edit restaurants</h1>
    <form method="post" action="/update/{{$edit->id}}" >
    @csrf
        <div class="col-sm-12">
            <div class="row">
                <div class="col-sm-6 mb-3">
                    <label>Name</label>
                    <input type="text" name="name" class="form-control" value="{{$edit->name}}" placeholder="Enter name">
                </div>
                <div class="col-sm-6 mb-3">
                    <label>Email</label>
                    <input type="email" name="email" class="form-control" value="{{$edit->email}}" placeholder="Enter email">
                </div>
            </div>
        </div>
        <div class="mb-3">
            <label>Address</label>
            <input type="text" name="address" class="form-control" value="{{$edit->address}}" placeholder="Enter address">
        </div>
        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
</div>
@stop