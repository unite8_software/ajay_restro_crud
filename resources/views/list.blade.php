@extends('layout')

@section('content')
<div>
    <h1>List of restaurants</h1>
@if(Session::get('status'))
<div class="alert {{Session::get('alert')}} alert-dismissible fade show" role="alert">
  <strong>  {{Session::get('status')}} </strong>
  <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
</div>
@endif

</div>
<table class="table">
    <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col">Name</th>
            <th scope="col">Email</th>
            <th scope="col">Address</th>
            <th scope="col">Operations</th>
        </tr>
    </thead>
    <tbody>
        @foreach($list as $item)
        <tr>
            <th scope="row">{{$item->id}}</th>
            <td>{{$item->name}}</td>
            <td>{{$item->email}}</td>
            <td>{{$item->address}}</td>
            <td><a href="edit/{{$item->id}}"><i class="fa fa-pencil  fa-2x" style="color:lime"></i></a><a href="delete/{{$item->id}}"><i class="fa fa-trash fa-2x" style="color:red"></i></a></td>
        </tr>
        @endforeach
    </tbody>
</table>
@stop