@extends('layout')

@section('content')
<div>
    <h1>Add new restaurants</h1>
    <form action="add" method="post">
    @csrf
        <div class="col-sm-12">
            <div class="row">
                <div class="col-sm-6 mb-3">
                    <label>Name</label>
                    <input type="text" name="name" class="form-control" placeholder="Enter name">
                </div>
                <div class="col-sm-6 mb-3">
                    <label>Email</label>
                    <input type="email" name="email" class="form-control" placeholder="Enter email">
                </div>
            </div>
        </div>
        <div class="mb-3">
            <label>Address</label>
            <input type="text" name="address" class="form-control" placeholder="Enter address">
        </div>
        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
</div>
@stop