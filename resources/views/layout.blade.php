<html>

<head>
    <title>Restaurant App</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <script src="https://code.jquery.com/jquery-3.5.1.js" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/js/bootstrap.bundle.min.js" crossorigin="anonymous"></script>
    <style>
        a#loggedin {
            color: lime;
        }

        a#loggedout {
            color: orangered;
        }
    </style>
</head>

<body>
    <header>
        <nav class="navbar navbar-expand-sm navbar-light bg-light">
            <div class="container-fluid">
                <a class="navbar-brand" href="#">Restro</a>
                <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarNav">
                    <ul class="navbar-nav">
                        <li class="nav-item">
                            <a class="nav-link active" aria-current="page" href="/">Home</a>
                        </li>
                        @if(Session::get('user'))
                        <li class="nav-item">
                            <a class="nav-link" href="/list">List</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="/add">Add</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="/" id="loggedin">Welcome {{Session::get('user')}}</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="/logout" id="loggedout">Logout</a>
                        </li>
                        @else
                        <li class="nav-item">
                            <a class="nav-link" href="/login">Login</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="/register">Register</a>
                        </li>
                        @endif
                    </ul>
                </div>
            </div>
        </nav>
    </header>
    <div class="container"> @yield('content') </div>
    <!-- <footer> Copyrights by restro app </footer> -->
</body>

</html>