<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Restaurant;
use App\Session;
use App\User;
use Illuminate\Support\Facades\Crypt;

class RestroController extends Controller
{
    //
    function index()
    {
        return view("home");
    }

    function list()
    {
        $resp = Restaurant::all();
        return view("list", ["list" => $resp]);
    }

    function add(Request $req)
    {
        $restro = new Restaurant;
        $restro->name = $req->input('name');
        $restro->email = $req->input('email');
        $restro->address = $req->input('address');
        $restro->save();
        $req->session()->flash('status', 'Restaurant added successfully');
        $req->session()->flash('alert','alert-success');
        return redirect('list');
    }

    function delete($id)
    {
        $delId = Restaurant::find($id)->delete();
        if (!empty($delId)) {
            Session()->flash('status', 'Restaurant deleted successfully');
            Session()->flash('alert','alert-danger');
            return redirect('list');
        }
    }

    function edit($id)
    {
        $resp = Restaurant::find($id);
        return view('edit', ['edit' => $resp]);
    }

    function update(Request $req,$id) 
    {
        $restro = Restaurant::find($id);
        $restro->name = $req->input('name');
        $restro->email = $req->input('email');
        $restro->address = $req->input('address');
        $restro->save();
        $req->session()->flash('status', 'Restaurant updated successfully');
        $req->session()->flash('alert','alert-warning');
        return redirect('list');
    }

    function register(Request $req)
    {
        
        $user = new User;
        $user->name = $req->input('name');
        $user->email = $req->input('email');
        $user->password = Crypt::encrypt($req->input('password'));
        $user->contact = $req->input('contact');
        $reg = $user->save();
        if(!empty($reg))
        $req->session()->put('user',$req->input('name'));
        return redirect('/');
    }

    function login(Request $req)
    {
        $userInfo = User::where("email", $req->input('email'))->get();
        if(Crypt::decrypt($userInfo[0]->password)  === $req->input('password'))
        {
            $req->session()->put('user',$userInfo[0]->name);
            return redirect('/');
        }
        else {
            $req->session()->flash('status','Login failed');
            return redirect('/login');
        }
    }

    function logout()
    {
        Session()->forget('user');
        return redirect('/');
    }
}
