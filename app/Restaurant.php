<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Restaurant extends Model
{
    //
    public function getEmailAttribute($value)
    {
        return ucwords($value);
    }
}
